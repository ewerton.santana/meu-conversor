import 'package:dio/dio.dart';

class CurrencyRepository {
  final countryCurrencies = ["USD", "EUR", "GBP", "ARS", "BTC"];
  final Dio dio = Dio(BaseOptions(
    baseUrl: "https://api.hgbrasil.com/finance?format=json&key=4177407c",
    contentType: "application/json",
  ));

  Future<List<Currency>> getCurrencies() async {
    final response = await dio.get("");
    final actualCurrencies = response.data["results"]["currencies"];
    return countryCurrencies.map<Currency>((currency) {
      var actualCurrency = actualCurrencies[currency];
      return Currency(actualCurrency["name"],actualCurrency["buy"], currency);
    }).toList();
  }
}

class Currency{
  String name;
  double value;
  String code;
  Currency(this.name, this.value, this.code);
}
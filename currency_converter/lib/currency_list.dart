import 'package:currency_converter/currency_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';

import 'conversion.dart';

class CurrencyList extends StatefulWidget {
  @override
  _CurrencyListState createState() => _CurrencyListState();
}

class _CurrencyListState extends State<CurrencyList> {
  List<Currency> countryCurrencies = [];

  @override
  void initState() {
    CurrencyRepository().getCurrencies().then((value) => {
          setState(() {
            countryCurrencies = value;
          })
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 12,
              right: 12,
              top: 40,
            ),
            height: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Escolha a moeda: ", style: TextStyle(fontSize: 25, color: Colors.black)),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: countryCurrencies.length,
                  itemBuilder: (context, index) {
                    final currency = countryCurrencies[index];
                    return GestureDetector(
                      onTap: () =>
                          {Navigator.push(context, MaterialPageRoute(builder: (context) => Conversion(currency)))},
                      child: Card(
                        margin: EdgeInsets.only(bottom: 12),
                        elevation: 10,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                currency.code,
                                style: TextStyle(fontSize: 25),
                              ),
                              Text(
                                currency.name,
                                style: TextStyle(fontSize: 15),
                              ),
                              Text("Cotação atual: ${realCurrencyConvert(currency.value)}"),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Icon(
                  Icons.keyboard_return,
                  size: 50,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

String realCurrencyConvert(double value) {
  return FlutterMoneyFormatter(
          amount: value,
          settings:
              MoneyFormatterSettings(symbol: "R\$", thousandSeparator: ',', decimalSeparator: '.', fractionDigits: 2))
      .output
      .symbolOnLeft;
}

String currencyConvert(double value) {
  return FlutterMoneyFormatter(
          amount: value,
          settings:
              MoneyFormatterSettings(symbol: "\$", thousandSeparator: ',', decimalSeparator: '.', fractionDigits: 2))
      .output
      .symbolOnLeft;
}

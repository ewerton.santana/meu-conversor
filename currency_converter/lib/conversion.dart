import 'package:currency_converter/currency_list.dart';
import 'package:currency_converter/currency_repository.dart';
import 'package:flutter/material.dart';

class Conversion extends StatefulWidget {
  final Currency currency;

  const Conversion(this.currency, {Key key}) : super(key: key);

  @override
  _ConversionState createState() => _ConversionState();
}

class _ConversionState extends State<Conversion> {
  double realValue = 1.0;
  TextEditingController realController = TextEditingController();
  double anotherValue = 0.0;
  TextEditingController anotherController = TextEditingController();

  @override
  void initState() {
    anotherValue = widget.currency.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              top: MediaQuery.of(context).size.height * 0.05,
              child: Text("Moedas: ", style: TextStyle(fontSize: 25, color: Colors.black))),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.20,
            left: 0,
            right: 0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Real", style: TextStyle(fontSize: 20)),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.25,
                  child: TextField(
                    controller: realController..text = realCurrencyConvert(realValue),
                    onChanged: (value) => setState(() {
                      realValue = double.parse(value.split(" ")[1]);
                      anotherValue = realValue / widget.currency.value;
                    }),
                  ),
                ),
                Icon(Icons.compare_arrows),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.currency.name,
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.25,
                  child: TextField(
                    controller: anotherController..text = currencyConvert(anotherValue),
                    onChanged: (value) => setState(() {
                      anotherValue = double.parse(value.split(" ")[1]);
                      realValue = anotherValue * widget.currency.value;
                    }),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
